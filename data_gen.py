import random
from geopy.geocoders import Nominatim
import geopy.geocoders
from geopy.extra.rate_limiter import RateLimiter
from geopy.exc import GeocoderTimedOut
import json



PROXIES = [
        ]
def get_data():
    full_data= []
    # coordinate = [52.5094982, 13.3765983]
    # sequence = [-1, 1]
    coordinate = [0, 0]
    try:
        # geopy.geocoders.options.default_proxies = random.choice(PROXIES)
        geolocator = Nominatim(user_agent="sair206")
        geocode = RateLimiter(geolocator.reverse, min_delay_seconds=1)
    except GeocoderTimedOut as e:
        print("Error: geocode failed on input with message %s" % e.message)
        _json(full_data)

    for i in range(1000):
        try:
            coordinate[0] = round(random.uniform(-90, 90), 7)
            coordinate[1] = round(random.uniform(-180, 180), 7)
            # coordinate[0] = float(format(coordinate[0] + random.choice(sequence) *  random.random())) % 90
            # coordinate[1] = float(format(coordinate[1] + random.choice(sequence) *  random.random())) % 180
            name = geocode(coordinate).address
            if name:
                full_data.append({'coordinate': (coordinate[0], coordinate[1]), 'name': name})
        except:
            pass
    _json(full_data)

def _json(full_data):
    path = 'test.json'
    with open(path, 'r', encoding='utf-8') as infile:
        data = json.load(infile)
    full_data.extend(data)
    with open(path, 'w', encoding='utf-8') as outfile:
        json.dump( full_data, outfile, ensure_ascii=False)


get_data()
