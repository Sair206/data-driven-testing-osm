import random
from geopy.geocoders import Nominatim
import geopy.geocoders
from geopy.extra.rate_limiter import RateLimiter
from geopy.exc import GeocoderTimedOut
import json
import unittest



# PATH = 'test.json'
PATH = 'test_small.json'

def frange(left, right, value):
    if left <= value <= right:
        return True
    else:
        False

def _json():
    with open(PATH, 'r', encoding='utf-8') as infile:
        data = json.load(infile)
    return data


class Testing(unittest.TestCase):


    def api_get_data(self, info, flag):
        try:
            geolocator = Nominatim(user_agent="sair206")
            raw_func = RateLimiter(geolocator.geocode, min_delay_seconds=1) if flag else \
                RateLimiter(geolocator.reverse, min_delay_seconds=1)
        except GeocoderTimedOut as e:
            print("Error: geocode failed on input with message %s" % e.message)

        names = raw_func(info, exactly_one=False)
        return names if names else []

    def check_coordinate(self, coordinate, names):
        for name in names:
            raw_coordinates = name.raw['boundingbox']
            if frange(float(raw_coordinates[0]), float(raw_coordinates[1]), coordinate[0]) and \
                    frange(float(raw_coordinates[2]), float(raw_coordinates[3]), coordinate[1]):
                return True
        return False

    def check_address(self, address, names):
        for name in names:
            raw_address = name.raw['display_name']
            if raw_address == address:
                return True
        return False

    # @unittest.skip("skipping")
    def test_forward_geocoding(self):
        for i, coordinate in enumerate(_json()):
            with self.subTest(i=i, name=coordinate['name']):
                self.assertTrue(self.check_coordinate((coordinate['coordinate'][0], coordinate['coordinate'][1]),
                                                      self.api_get_data(coordinate['name'], 1)))

    # @unittest.skip("skipping")
    def test_reverse_geocoding(self):
        for i, coordinate in enumerate(_json()):
            with self.subTest(i=i, name=coordinate['name']):
                self.assertTrue(self.check_address(coordinate['name'], self.api_get_data(
                    (coordinate['coordinate'][0], coordinate['coordinate'][1]), 0)))


if __name__ == 'main':
    unittest.main









